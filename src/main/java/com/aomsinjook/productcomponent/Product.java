/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aomsinjook.productcomponent;

import java.util.ArrayList;

/**
 *
 * @author WINDOWS 10
 */
public class Product {
   private int id;
   private String name;
   private double price;
   private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    public static ArrayList<Product> genProductList(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1,"Esspresso 1",40,"p1.jpg"));
        list.add(new Product(2,"Esspresso 2",40,"p2.jpg"));
        list.add(new Product(3,"Esspresso 3",40,"p3.jpg"));
        list.add(new Product(4,"Americano 1",40,"p1.jpg"));
        list.add(new Product(5,"Americano 2",40,"p2.jpg"));
        list.add(new Product(1,"Americano 3",40,"p3.jpg"));
        list.add(new Product(1,"Chayen 1",40,"p1.jpg"));
        list.add(new Product(1,"Chayen 2",40,"p2.jpg"));
        list.add(new Product(1,"Chayen 3",40,"p3.jpg"));
        return list;
    }        
}
